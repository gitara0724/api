const Cart = require("../models/Cart")
const auth = require("../auth")



module.exports.addItem = (req, res) => {
    const token = req.headers.authorization
    const userInst = auth.decode(token)

    if(!userInst.isAdmin){
    const productId = req.params.productId

    const cartInst = new Cart({
        product: productId,
        user: userInst.id,
        quantity: req.body.quantity || 0,
    })

    cartInst.save().then((data) => {
        res.send({
            message: "Item added to cart",
            data: data,
        })
    })
    }
    else{
        return res.send("You are admin, you cannot proceed to shopping.")
    }
}